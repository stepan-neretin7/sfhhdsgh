#include <regex>
#include "converters/ConverterFactory.h"
#include "ConfigParser.h"
#include "helpers/LineHelper.h"

const inline char commentSign = '#';
const inline char fileSign = '$';

std::vector<std::string> slicingFirst(std::vector<std::string> const &v) {
  auto first = v.begin() + 1;
  auto last = v.end();

  std::vector<std::string> vector(first, last);
  return vector;
}

std::vector<std::string> splitStringByMatches(const std::string &s, const std::regex &re) {
  std::vector<std::string> elems;

  for (auto it = std::sregex_iterator(s.begin(), s.end(), re);
       it != std::sregex_iterator(); it++) {
    const std::smatch &match = *it;
    elems.push_back(match.str(0));
  }

  return elems;
}

ConfigParser::ConfigParser(const std::string &name) {
  this->file.open(name);
  if (!this->file.is_open()) throw std::runtime_error("Could not open file");
}
std::vector<std::unique_ptr<Converters::Converter>> ConfigParser::parseConfig(std::vector<std::string> files) {
  auto converterFactory = Converters::ConverterFactory();
  auto res = std::vector<std::unique_ptr<Converters::Converter>>();
  std::string line;
  std::regex re(R"([a-zA-Z0-9_.-\\$]+)");

  while (getline(file, line)) {
    if (line.empty()) {
      continue;
    }

    if (Helpers::trim(line).find(commentSign) == 0) {
      continue;
    }

    auto tokens = splitStringByMatches(line, re);
    if (tokens.empty()) {
      continue;
    }

    for (auto &token : tokens) {
      if (Helpers::trim(token).find(fileSign) == 0) {
        auto index = std::stoi(Helpers::trim(token).erase(0, 1));
        if (static_cast<size_t>(index) >= files.size() || index <= 0) {
          throw std::invalid_argument(
              "Index: {" + std::to_string(index) + "} does not exist in the files which you transferred.");
        }
        token = "$" + files.at(index);
      }
    }

    auto newConverter = converterFactory.create(tokens[0]);
    newConverter->setConfig(slicingFirst(tokens));
    res.push_back(std::move(newConverter));
  }

  return res;
}
