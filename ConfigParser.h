#pragma once
#include <string>
#include <memory>
#include "converters/Converter.h"
#include <fstream>

class ConfigParser {
 public:
    explicit ConfigParser(const std::string &name);
    std::vector<std::unique_ptr<Converters::Converter>> parseConfig(std::vector<std::string> files);
 private:
    std::ifstream file;
};
