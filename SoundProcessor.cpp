
#include <memory>
#include "WavWriter.h"
#include "SoundProcessor.h"

const std::unique_ptr<WavWriter> &SoundProcessor::getWavWriter() const {
  return wavWriter;
}

ParsedWavInfo SoundProcessor::parseWavFileInfo(std::istream &wavFile) {
  return WavParser::parseWavFile(wavFile);
}
SoundProcessor::SoundProcessor(const std::filesystem::path &outName) {
  wavWriter = std::make_unique<WavWriter>(outName);
}
