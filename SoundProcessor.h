#pragma once
#include <iostream>
#include <filesystem>
#include <vector>
#include "WavParser.h"
#include "WavWriter.h"

class SoundProcessor {
 public:
    explicit SoundProcessor(const std::filesystem::path &outName);
    [[nodiscard]] const std::unique_ptr<WavWriter> &getWavWriter() const;
    static ParsedWavInfo parseWavFileInfo(std::istream &wavFile);

    static int16_t readSample(std::istream &in, const ParsedWavInfo &info) {
      if (in.eof()) {
        throw std::runtime_error("Samples count incorrect");
      }

      int16_t sample;

      in.read(reinterpret_cast<char *>(&sample), info.bytesPerSample);

      if (in.gcount() < info.bytesPerSample) {
        throw std::runtime_error("something went wrong");
      }

      return sample;
    }

    static void prepare(std::istream &in, const ParsedWavInfo &info) {
      in.seekg(info.dataPosition, std::fstream::beg);
    }

 private:
    std::unique_ptr<WavWriter> wavWriter;
};

