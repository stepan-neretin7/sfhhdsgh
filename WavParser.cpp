#include <iostream>
#include <cstring>
#include "WavParser.h"

const inline uint32_t supportSampleRate = 44100;
const inline uint32_t supportAudioFormat = 1;
const inline uint32_t supportNumChannels = 1;
const inline uint32_t supportBitsPerSample = 16;

const inline char WAVE_FORMAT[] = "WAVE";

void read(std::istream &f, void *buffer, std::streamsize size) {
  if (!f.read(static_cast<char *>(buffer), size)) {
    return;
  }
}

void read(std::istream &f, ChunkInfo &c) {
  read(f, &c.chunkId, 4);
  read(f, &(c.chunkSize), 4);
}

[[maybe_unused]] std::string convert4ByteUIntToString(uint32_t n) {
  return {
      char(n & 0xFF),
      char((n >> 8) & 0xFF),
      char((n >> 16) & 0xFF),
      char((n >> 24) & 0xFF)
  };
}

WavRiffHeader WavParser::parseRiffChunk(std::istream &rawWavFile) {
  auto riff = WavRiffHeader{};
  read(rawWavFile, riff.riff);

  if (riff.riff.chunkId != CT_RIFF) {
    throw std::runtime_error("First chunk must be RIFF");
  }

  rawWavFile.read(static_cast<char *>(riff.format), uint32Size);

  if (std::memcmp(WAVE_FORMAT, riff.format, 4) != 0) {
    throw std::invalid_argument("Riff format must be WAVE");
  }

  return riff;
}

ParsedWavInfo WavParser::parseWavFile(std::istream &rawWavFile) {
  auto chunk = ChunkInfo{};
  auto res = ParsedWavInfo{};

  auto fmtChunk = WavFmtHeader{};
  bool isFmtFound = false;

  res.riffHeader = parseRiffChunk(rawWavFile);

  while (rawWavFile) {
    read(rawWavFile, chunk);
    uint32_t next_chunk = static_cast<size_t>(rawWavFile.tellg()) + chunk.chunkSize + (chunk.chunkSize & 1);

    if (chunk.chunkId == CT_fmt) {
      res.fmtChunkInfo = chunk;
      rawWavFile.read(reinterpret_cast<char *>(&fmtChunk), sizeof(fmtChunk));
      isFmtFound = true;
    }

    if (chunk.chunkId == CT_data) {
      res.dataPosition = rawWavFile.tellg();
      res.dataSize = chunk.chunkSize;
      break;
    }

    rawWavFile.seekg(next_chunk);
  }

  if (!isFmtFound) {
    throw std::runtime_error("fmt not found");
  }

  res.fmtHeader = fmtChunk;

  res.bytesPerSample = res.fmtHeader.bitsPerSample / 8;
  res.samplesCount = res.dataSize / (res.bytesPerSample);
  return res;
}

void WavParser::validateFmtChunk(WavFmtHeader header) {
  if (header.numChannels != supportNumChannels) {
    throw std::invalid_argument(
        "Invalid num channels. We support only " + std::to_string(supportNumChannels) + " channels in wav file.");
  }
  if (header.audioFormat != supportAudioFormat) {
    throw std::invalid_argument("Invalid audioFormat. We support only PCM");
  }
  if (header.sampleRate != supportSampleRate) {
    throw std::invalid_argument("Invalid SampleRate. We support only " + std::to_string(supportSampleRate));
  }

  if (header.bitsPerSample != supportBitsPerSample) {
    throw std::invalid_argument("Invalid bitsPerSample. We support only " + std::to_string(supportBitsPerSample));
  }
}
