#pragma once
#include <fstream>
#include "WavFormat.h"

struct ParsedWavInfo {
  WavRiffHeader riffHeader;
  ChunkInfo fmtChunkInfo;
  WavFmtHeader fmtHeader;
  uint32_t dataSize; // NumSamples * NumChannels * BitsPerSample/8
  std::streampos dataPosition;
  std::string fileName;
  size_t samplesCount;
  uint32_t bytesPerSample;
};

class WavParser {
 public:
    static ParsedWavInfo parseWavFile(std::istream &rawWavFile);
 private:
    static WavRiffHeader parseRiffChunk(std::istream &rawWavFile);
    static void validateFmtChunk(WavFmtHeader header);
};
