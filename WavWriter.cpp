#include <memory>
#include "WavWriter.h"

WavWriter::WavWriter(const std::filesystem::path &outName) {
  out = std::fstream(outName, std::fstream::out | std::fstream::binary);
  if (out.fail()) {
    throw std::runtime_error("out file can`t opened");
  }
}

void WavWriter::writeWavHeader(const ParsedWavInfo &info) {
  out.write(reinterpret_cast<const char *>(&info.riffHeader), sizeof(info.riffHeader));
  out.write(reinterpret_cast<const char *>(&info.fmtChunkInfo), sizeof(info.fmtChunkInfo));
  out.write(reinterpret_cast<const char *>(&info.fmtHeader), sizeof(info.fmtHeader));
}

void WavWriter::writeWavDataChunkInfo(uint32_t dataSize) {
  auto dataChunk = ChunkInfo{};
  dataChunk.chunkSize = dataSize;
  dataChunk.chunkId = CT_data;
  out.write(reinterpret_cast<const char *>(&dataChunk), sizeof(dataChunk));
}

void WavWriter::writeSample(int16_t sample) {
  out.write(reinterpret_cast<const char *>(&sample), 2);
}
void WavWriter::seekToDataChunk(std::streampos pos) {
  out.seekg(pos, std::fstream::beg);
}
