#pragma once
#include "WavParser.h"
#include <filesystem>

class WavWriter {
 public:
    explicit WavWriter(const std::filesystem::path &outName);
    void writeWavHeader(const ParsedWavInfo &info);
    void writeWavDataChunkInfo(uint32_t dataSize);
    void seekToDataChunk(std::streampos pos);
    void writeSample(int16_t sample);
 private:
    std::fstream out;
};
