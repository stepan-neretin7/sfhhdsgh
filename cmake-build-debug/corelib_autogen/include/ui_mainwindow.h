/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *pushButton;
    QPushButton *filesPushButton;
    QLabel *files;
    QLabel *label;
    QPushButton *configPushButton;
    QLabel *config;
    QLineEdit *outName;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(788, 602);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(320, 390, 281, 101));
        filesPushButton = new QPushButton(centralwidget);
        filesPushButton->setObjectName(QString::fromUtf8("filesPushButton"));
        filesPushButton->setGeometry(QRect(1, 1, 130, 28));
        files = new QLabel(centralwidget);
        files->setObjectName(QString::fromUtf8("files"));
        files->setGeometry(QRect(137, 1, 651, 21));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(137, 69, 16, 16));
        configPushButton = new QPushButton(centralwidget);
        configPushButton->setObjectName(QString::fromUtf8("configPushButton"));
        configPushButton->setGeometry(QRect(1, 35, 102, 28));
        config = new QLabel(centralwidget);
        config->setObjectName(QString::fromUtf8("config"));
        config->setGeometry(QRect(130, 40, 651, 21));
        outName = new QLineEdit(centralwidget);
        outName->setObjectName(QString::fromUtf8("outName"));
        outName->setGeometry(QRect(10, 80, 751, 28));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 788, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "PushButton", nullptr));
        filesPushButton->setText(QCoreApplication::translate("MainWindow", "input files(choose)", nullptr));
        files->setText(QString());
        label->setText(QString());
        configPushButton->setText(QCoreApplication::translate("MainWindow", "choose config", nullptr));
        config->setText(QString());
        outName->setInputMask(QString());
        outName->setText(QCoreApplication::translate("MainWindow", "Input out input fileName", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
