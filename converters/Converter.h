#pragma once
#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include "../WavParser.h"
#include "../WavWriter.h"
#include "../SoundProcessor.h"

namespace Converters {

class Converter {
 public:
    virtual void transformSound(std::fstream in, const SoundProcessor &processor) = 0;
    [[nodiscard]] virtual std::string getConverterDescription() const = 0;

    void setConfig(std::vector<std::string> argsVector) {
      this->args = std::move(argsVector);
    }


    virtual ~Converter() = default;
 protected:
    std::vector<std::string> args;

    static std::string parseDashedArg(std::string arg) {
      if (arg[0] != '$') {
        throw std::invalid_argument("dashed arg must be start with $");
      }
      arg.erase(0, 1);
      return arg;
    }

};

}