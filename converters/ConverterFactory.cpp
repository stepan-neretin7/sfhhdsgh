#include <memory>
#include <map>
#include <stdexcept>
#include "ConverterFactory.h"
#include "Mute.h"
#include "Mix.h"
#include "Echo.h"

namespace Converters {

ConverterFactory::ConverterFactory() {
  registerStrategies();
}

void ConverterFactory::registerStrategy(std::string_view name, std::function<std::unique_ptr<Converter>()> creator) {
  factories[name] = std::move(creator);
}

std::unique_ptr<Converter> ConverterFactory::create(std::string_view strategyName) {
  auto it = factories.find(strategyName);
  if (it != factories.end()) {
    return it->second();
  }
  throw std::runtime_error("cannot create object: unknown class name: " + std::string(strategyName));
}
void ConverterFactory::registerStrategies() {
  registerStrategy("mute", []() {
    return std::make_unique<Mute>();
  });

  registerStrategy("mix", []() {
    return std::make_unique<Mix>();
  });

  registerStrategy("echo", []() {
    return std::make_unique<Echo>();
  });
}

std::map<std::string_view, std::string> ConverterFactory::getElementsInfo() {
  auto res = std::map<std::string_view, std::string>();
  for (auto const &key : factories) {
    res.try_emplace(key.first, getDescription(key.first));
  }
  return res;
}

std::string ConverterFactory::getDescription(const std::string_view &converterName) {
  return create(converterName)->getConverterDescription();
}

}