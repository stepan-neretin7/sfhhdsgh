#pragma once
#include <map>
#include <functional>
#include "Converter.h"
#include <memory>

namespace Converters {

using FactoriesMap = std::map<std::string_view, std::function<std::unique_ptr<Converter>()>>;

class ConverterFactory {
 public:
    ConverterFactory();
    [[nodiscard]] std::unique_ptr<Converter> create(std::string_view strategyName);
    [[nodiscard]] std::map<std::string_view, std::string> getElementsInfo();
 private:
    std::string getDescription(const std::string_view &converterName);
    void registerStrategy(std::string_view name, std::function<std::unique_ptr<Converter>()> creator);
    void registerStrategies();
    FactoriesMap factories;
};

}
