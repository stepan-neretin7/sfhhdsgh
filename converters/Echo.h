#pragma once
#include <cstring>
#include <queue>
#include "Converter.h"
#include "../WavWriter.h"

namespace Converters {

class Echo : public Converter {

    [[nodiscard]] std::string getConverterDescription() const override{
      return "echo converter";
    }

    void transformSound(std::fstream in, const SoundProcessor &processor) override {
      if (in.fail()) {
        throw std::runtime_error("opening file failed");
      }

      if (args.size() != 3) {
        throw std::invalid_argument("Invalid arguments size for echo converter. It should be 3(start, stop, delay sec)");
      }

      auto delaySecs = std::stof(args[2]);

      auto info = SoundProcessor::parseWavFileInfo(in);

      size_t secondsLengthFile = info.samplesCount / info.fmtHeader.sampleRate;
      size_t samplesInSecond = info.samplesCount / secondsLengthFile;

      size_t from = std::stoi(args[0]) * samplesInSecond;
      size_t to = std::stoi(args[1]) * samplesInSecond;

      auto delaySamples = static_cast<size_t>(static_cast<float>(info.fmtHeader.sampleRate) * delaySecs) / 10;

      auto ringBuffer = std::make_unique<int16_t[]>(delaySamples);
      std::memset(ringBuffer.get(), 0, sizeof(int16_t) * delaySamples);

      SoundProcessor::prepare(in, info);

      info.riffHeader.riff.chunkSize = static_cast<uint32_t>(info.samplesCount * info.bytesPerSample + 36);
      processor.getWavWriter()->writeWavHeader(info);
      processor.getWavWriter()->writeWavDataChunkInfo(info.dataSize);
      processor.getWavWriter()->seekToDataChunk(info.dataPosition);

      for (size_t i = 0; i < info.samplesCount; i++) {
        auto sample = static_cast<double>(SoundProcessor::readSample(in, info));
        if (i >= from && i <= to) {
          sample += static_cast<double>(ringBuffer[i % delaySamples]) / 2;
          sample = (sample < -32768) ? -32768 : (sample > 32767) ? 32767 : sample;
          ringBuffer[i % delaySamples] = static_cast<int16_t>(sample);
        }
        processor.getWavWriter()->writeSample(static_cast<int16_t>(sample));
      }
    }
};

}