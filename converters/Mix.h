#pragma once
#include <cstring>
#include "Converter.h"
#include "../WavWriter.h"

namespace Converters {

class Mix : public Converter {

    [[nodiscard]] std::string getConverterDescription() const override{
      return "Mix converter";
    }

    void transformSound(std::fstream in, const SoundProcessor &processor) override {
      if (in.fail()) {
        throw std::runtime_error("opening file failed");
      }

      auto info = SoundProcessor::parseWavFileInfo(in);
      SoundProcessor::prepare(in, info);
      size_t secondsLengthFile = info.samplesCount / info.fmtHeader.sampleRate;
      size_t samplesInSecond = info.samplesCount / secondsLengthFile;

      auto mixingFileName = parseDashedArg(args[0]);
      auto mixingFile = std::ifstream(mixingFileName);
      auto mixingFileInfo = SoundProcessor::parseWavFileInfo(mixingFile);

      if (args.size() < 2) {
        if (args.empty()) {
          throw std::invalid_argument("Invalid arguments size for mix converter. It should be minium 1");
        }
        args.emplace_back("0");
      }

      size_t fromPoint = std::stoi(args[1]) * samplesInSecond;

      info.riffHeader.riff.chunkSize = static_cast<uint32_t>(info.samplesCount * info.bytesPerSample + 36);
      processor.getWavWriter()->writeWavHeader(info);
      processor.getWavWriter()->writeWavDataChunkInfo(info.dataSize);
      processor.getWavWriter()->seekToDataChunk(info.dataPosition);

      for (size_t i = 0; i < info.samplesCount; i++) {
        int16_t sample = SoundProcessor::readSample(in, info);

        if (i >= fromPoint) {
          try {
            auto mixSample = SoundProcessor::readSample(mixingFile, mixingFileInfo);
            sample = static_cast<int16_t>((sample + mixSample) / 2);
          } catch (const std::runtime_error &e) {}
          catch (const std::exception &e) {
            throw e;
          }

        }

        processor.getWavWriter()->writeSample(sample);
      }
    }

};

}