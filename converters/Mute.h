#pragma once
#include <cstring>
#include "Converter.h"
#include "../WavWriter.h"

namespace Converters {

class Mute : public Converter {

    [[nodiscard]] std::string getConverterDescription() const override{
      return "Mute converter";
    }

    void transformSound(std::fstream in, const SoundProcessor &processor) override {
      if (in.fail()) {
        throw std::runtime_error("opening file failed");
      }

      auto info = SoundProcessor::parseWavFileInfo(in);
      SoundProcessor::prepare(in, info);
      size_t secondsLengthFile = info.samplesCount / info.fmtHeader.sampleRate;
      size_t samplesInSecond = info.samplesCount / secondsLengthFile;

      if (args.size() != 2) {
        throw std::invalid_argument("Invalid arguments size for mute converter. It should be 2(from, to)");
      }
      size_t from = std::stoi(args[0]) * samplesInSecond;
      size_t to = std::stoi(args[1]) * samplesInSecond;

      info.riffHeader.riff.chunkSize = static_cast<uint32_t>(info.samplesCount * info.bytesPerSample + 36);
      processor.getWavWriter()->writeWavHeader(info);
      processor.getWavWriter()->writeWavDataChunkInfo(info.dataSize);
      processor.getWavWriter()->seekToDataChunk(info.dataPosition);

      for (size_t i = 0; i < info.samplesCount; i++) {
        int16_t sample = SoundProcessor::readSample(in, info);

        if (i >= from && i <= to) {
          sample = 0;
        }

        processor.getWavWriter()->writeSample(sample);
      }
    }

};

}