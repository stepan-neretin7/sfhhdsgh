#include <QFileDialog>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVector>
#include <vector>

std::vector<std::string> QStringList_to_VectorString(const QList<QString>& qlist) {
	std::vector<std::string> result(qlist.size());
	for (int i=0; i<qlist.size(); i++) {
		result[i] = qlist.at(i).toUtf8().data();
	}
	return result;
}

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
	this->setFixedSize(QSize(900, 500));
    connect(ui->filesPushButton, &QPushButton::clicked, this, &MainWindow::on_pushButtonFiles_clicked);
    connect(ui->configPushButton, &QPushButton::clicked, this, &MainWindow::on_pushButtonConfig_clicked);
	connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::on_pushButton_clicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButtonFiles_clicked()
{
	QStringList files = QFileDialog::getOpenFileNames(
			this,
			"Select one or more files to open");
	ui->files->setText(QString::fromStdString("Files choose: " + std::to_string(files.size())));
	QMessageBox::information(this, tr("File name"), files.join(","));
	inputFileNames = QStringList_to_VectorString(files);

}

void MainWindow::on_pushButtonConfig_clicked(){
	QString filename=QFileDialog::getOpenFileName(this, tr("Open file"));
	QMessageBox::information(this, tr("File name"), filename);
	ui->config->setText(filename);
	configFileName = filename.toStdString();

}
void MainWindow::on_pushButton_clicked(){

	auto outName = ui->outName->text();
	if(inputFileNames.empty() || configFileName.empty()){
		QMessageBox::information(this, tr("File name"), "inputFileNames or configFileName size");
		return;
	}
	QMessageBox::information(this, tr("File name"), outName);
}

